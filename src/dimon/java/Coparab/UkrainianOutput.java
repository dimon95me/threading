package dimon.java.Coparab;

/**
 * Created by Dimon on 14.05.18.
 */
public class UkrainianOutput extends EnglishOutput {

    public UkrainianOutput() {
        valueNames.put("temperature","темперaтура");
        valueNames.put("pressure","тиск");
        valueNames.put("humidity","вологiсть");
        super.delay = 1200;
        super.indent = "\n\t\t\t";
        super.millis = Creator.lastUpdateMillis;
    }
}
