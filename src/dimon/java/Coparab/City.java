package dimon.java.Coparab;

/**
 * Created by Dimon on 14.05.18.
 */
public class City {
    private String cityName;
    private int temperature;
    private int pressure; //давление
    private int humidity; //влажность

    public City(String cityName, int temperature, int pressure, int humidity) {
        this.cityName = cityName;
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
    }

    public String getName() {
        return cityName;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getPressure() {
        return pressure;
    }

    public int getHumidity() {
        return humidity;
    }

}
