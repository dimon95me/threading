package dimon.java.Coparab;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Dimon on 14.05.18.
 */
public class Creator extends Thread {
    public static ConcurrentHashMap<String, City> weatherMap = new ConcurrentHashMap<String, City>();
    public static long lastUpdateMillis = System.currentTimeMillis();
    City city;
    Randomizer randomizer = new Randomizer();
//    String cityName;
//    int temperature;
//    int pressure; //давление
//    int humidity; //влажность
//
//    public Creator(String cityName, int temperature, int pressure, int humidity) {
//        this.cityName = cityName;
//        this.temperature = temperature;
//        this.pressure = pressure;
//        this.humidity = humidity;
//    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            city = new City(randomizer.generateName(),
                    randomizer.generateBetweenNumbers(-5, 35),
                    randomizer.generateBetweenNumbers(740, 765),
                    randomizer.generateBetweenNumbers(50, 85));

            weatherMap.put(city.getName(),city);
            lastUpdateMillis =System.currentTimeMillis();

//            System.out.println(city.getName()+"\ntemperature: "+
//                    city.getTemperature()+"\npressure: "+
//                    city.getPressure()+"\nhumidity: "+
//                    city.getHumidity()+"\n");
//            System.out.println(lastUpdateMillis+"\n");



            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (i==9){
                i=0;
            }
        }
    }
}
