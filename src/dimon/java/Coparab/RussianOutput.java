package dimon.java.Coparab;

/**
 * Created by Dimon on 14.05.18.
 */
public class RussianOutput extends EnglishOutput {
    public RussianOutput() {
        valueNames.put("temperature","темперaтура");
        valueNames.put("pressure","давление");
        valueNames.put("humidity","влажность");
        super.delay = 2000;
        super.indent = "\n\t\t\t\t\t\t";
        super.millis = Creator.lastUpdateMillis;
    }
}
