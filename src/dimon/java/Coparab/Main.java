package dimon.java.Coparab;

import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class Main {


    Randomizer randomizer = new Randomizer();

    public static void main(String[] args) {
	// write your code here
        Creator creator = new Creator();
        creator.start();

        EnglishOutput englishOutput = new EnglishOutput();
        englishOutput.start();

        UkrainianOutput ukrainianOutput = new UkrainianOutput();
        ukrainianOutput.start();

        RussianOutput russianOutput = new RussianOutput();
        russianOutput.start();
    }




}
