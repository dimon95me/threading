package dimon.java.Coparab;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dimon on 14.05.18.
 */
public class CountryOutput extends Thread{
    protected long millis = Creator.lastUpdateMillis;
    protected Map<String, String> valueNames = new HashMap<String, String>();
    protected int delay = 700;
    protected String indent = "\n";

    public CountryOutput() {
        valueNames.clear();
    }

    @Override
    public void run() {

        boolean infinity = true;
        while(infinity) {
            if (millis < Creator.lastUpdateMillis) {
                for (Map.Entry<String, City> entry : Creator.weatherMap.entrySet()) {
                    System.out.println(indent+entry.getValue().getName() + indent+ valueNames.get("temperature")+": " +
                            entry.getValue().getTemperature() + indent+ valueNames.get("pressure")+": " +
                            entry.getValue().getPressure() + indent+ valueNames.get("humidity")+": " +
                            entry.getValue().getHumidity());
                }

            }
            millis = Creator.lastUpdateMillis;
            try {
                sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }

    }
}
