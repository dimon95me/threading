package dimon.java.Coparab;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dimon on 14.05.18.
 */
public class EnglishOutput extends CountryOutput {

    public EnglishOutput() {
        valueNames.put("temperature","temperature");
        valueNames.put("pressure","pressure");
        valueNames.put("humidity","humidity");
        super.delay = 850;
        super.indent = "\n";
        super.millis = Creator.lastUpdateMillis;
    }


}
