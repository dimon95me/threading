package dimon.java.Coparab;

import java.util.Random;

/**
 * Created by Dimon on 14.05.18.
 */
public class Randomizer {
    private String[] cityNames = {"Hong kong", "Kyiv", "Moskow", "Praha", "Krakow", "Lublin"};
    private Random random = new Random();

    public String generateName (){

        return cityNames[random.nextInt(cityNames.length)];
    }

    public int generateBetweenNumbers(int min, int max){
        if (min >max){
            int a = max;
            max = min;
            min=a;
        }
        return random.nextInt(max-min)+min;
    }



}
